import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from "class-validator";
import { FlightDto } from './flight.dto';

export class MailDeclineDto extends FlightDto{
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  reservationStatus: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  reason: string;
}
