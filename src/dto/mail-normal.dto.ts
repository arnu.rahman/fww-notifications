import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsOptional } from "class-validator";
import { FlightDto } from './flight.dto';

export class MailNormalDto extends FlightDto{
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  boardingTime: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  typeCode: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  reservationCode: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  nextProcess: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  deadline: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  consequence: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  specialNote: string;
}
