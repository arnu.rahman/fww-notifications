import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from "class-validator";

export class FlightDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  passengerMail: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  flightCode: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  airplane: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  flightDate: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  deparAirport: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  deparTime: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  destiAirport: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  destiTime: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  seatNumber: string;
}
