import { EventPattern, Payload } from '@nestjs/microservices';
import { Controller,ForbiddenException,Logger,Post, UseGuards, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { MailDeclineDto } from './dto/mail-decline.dto';
import { MailNormalDto } from './dto/mail-normal.dto';
import { ApiBasicAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('Notifications')
@ApiBasicAuth()
@Controller({ path: 'mail', version: '1' })
export class AppController {
  private readonly logger = new Logger(AppController.name);
  constructor(private readonly notificationsService: AppService) {}

  //send mail by api
  @UseGuards(AuthGuard('basic'))
  @Post('send-decline')
  async sendDecline(@Body() data: MailDeclineDto) {
    this.logger.log('[GET] /api/v1/mail/send-decline');
    try {
      this.notificationsService.sendNotifDecline(data)
      this.logger.log('Send Notification Decline Done');

      return "Success send mail decline"
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Error in handleSendNotificationDecline');
    }
  }

  @UseGuards(AuthGuard('basic'))
  @Post('send-normal')
  async sendNormal(@Body() data: MailNormalDto) {
    this.logger.log('[GET] /api/v1/mail/send-normal');
    try {
      this.notificationsService.sendNotifNormal(data)
      this.logger.log('Send Notification Normal Done');

      return "Success send mail normal"
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Error in handleSendNotificationNormal');
    }
  }

  @EventPattern('EPM_SendDeclineMail')
  async handleSendNotificationDecline(@Payload() data: any){
    this.logger.log(`[EP] EPM_SendDeclineMail`);
    try {
      this.notificationsService.sendNotifDecline(data)
      this.logger.log('Send Notification Decline Done');
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Error in handleSendNotificationDecline');
    }
  }

  @EventPattern('EPM_SendNormalMail')
  async handleSendNotificationNormal(@Payload() data: any){
    this.logger.log(`[EP] EPM_SendNormalMail`);
    try {
      this.notificationsService.sendNotifNormal(data)
      this.logger.log('Send Notification Normal Done');
    } catch (error) {
      this.logger.error(error);
      throw new ForbiddenException('Error in handleSendNotificationNormal');
    }
  }
}
