import { BasicStrategy as Strategy } from 'passport-http';
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from '@nestjs/passport';
import * as md5 from 'md5'
import { ConfigService } from '@nestjs/config';

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly configService: ConfigService) {
        super({
            passReqToCallback: true
        });
    }

    public validate = async (_:Request, username:string, password:string): Promise<boolean> => {
        let username_access = this.configService.get<string>('BASIC_USER')
        let password_access = this.configService.get<string>('BASIC_PASS')

        //get data auth
        if (username_access === username && md5(password) === password_access) {
            return true;
        }
        throw new UnauthorizedException();
    }
}
