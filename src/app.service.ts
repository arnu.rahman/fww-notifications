import { Injectable } from '@nestjs/common';
import { Logger } from '@nestjs/common/services';
import * as handlebars from 'handlebars';
import * as nodemailer from 'nodemailer';
import * as fs from 'fs';
import * as path from 'path';
import * as dotenv from 'dotenv'
dotenv.config();
@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name)
  private transporter: nodemailer.Transporter;
  private confirmationTemplate: handlebars.TemplateDelegate;

  constructor() {
    this.transporter = nodemailer.createTransport(
      {
        host: process.env.MAIL_HOST,
        auth: {
          user: process.env.USER_MAIL,
          pass: process.env.PASS_MAIL,
        },
      },
      {
        from: {
          name: 'No-reply',
          address: process.env.MAIL_FROM,
        },
      },
    );
  }

  private loadTemplate(templateName: string): handlebars.TemplateDelegate {
    const templatesFolderPath = path.join(__dirname, './mails');
    const templatePath = path.join(templatesFolderPath, templateName);

    const templateSource = fs.readFileSync(templatePath, 'utf8');
    return handlebars.compile(templateSource);
  }

  async sendNotifDecline(data: any) {
    const payload = {
      reservationStatus: data.reservationStatus,
      reason: data.reason,
      flightCode: data.flightCode,
      flightDate: data.flightDate,
      airplane: data.airplane,
      deparAirport: data.deparAirport,
      deparTime: data.deparTime,
      destiAirport: data.destiAirport,
      destiTime: data.destiTime,
      seatNumber: data.seatNumber
    }
    this.confirmationTemplate = this.loadTemplate('mail-decline.hbs')
    const html = this.confirmationTemplate(payload);

    const sendNotif = await this.transporter.sendMail({
      to: data.passengerMail,
      subject: '[FWW Information] Decline of Reservation',
      html: html,
    });

    this.logger.log('Process send notification for decline process');
    return sendNotif
  }

  async sendNotifNormal(data: any) {
    const payload =  {
      flightCode: data.flightCode,
      flightDate: data.flightDate,
      airplane: data.airplane,
      boardingTime: data.boardingTime,
      deparAirport: data.deparAirport,
      deparTime: data.deparTime,
      destiAirport: data.destiAirport,
      destiTime: data.destiTime,
      seatNumber: data.seatNumber,
      typeCode: data.typeCode,
      reservationCode: data.reservationCode,
      nextProcess: data.nextProcess,
      deadline: data.deadline,
      consequence: data.consequence,
      specialNote: data.specialNote
    }
    this.confirmationTemplate = this.loadTemplate('mail-normal.hbs')
    const html = this.confirmationTemplate(payload);

    const sendNotif = await this.transporter.sendMail({
      to: data.passengerMail,
      subject: '[FWW Information] Reservation Status',
      html: html,
    });

    this.logger.log('Process send notification for normal process');
    return sendNotif
  }
}
